# Consumer Client IP

This module integrates with the `consumers` contrib-module to provide a way to
map the value of a given HTTP-Header to the `X-Forwarded-For` HTTP-Header value
for the incoming request.

This is useful to make the integrated flood handling work when e.g. the request
comes from a serverless function which passes the client's ip addres in e.g.
the `X-Client-IP` header.

## Configuration

The header rewrite is done on a per consumer basis.

The functionality can be enabled on the settings form for each consumer entity.
The name of the http header to retrieve the client ip from must also be set
in the consumer configuration.

# Consumer Client IP

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

This module integrates with the `consumers` contrib-module to provide a way to
map the value of a given HTTP-Header to the `X-Forwarded-For` HTTP-Header value
for the incoming request.

### Why is this needed?

In decoupled environments, the actual client (browser) might not directly
connect to the Drupal server.

Instead, the client might connect to the frontend application (e.g. Next.js)
which then connects to the Drupal server.

In this case, the Drupal server will not be able to determine the client's IP
address, because the IP address of the request is the address of the frontend
application, not the client.

To solve this, the frontend application can send the client's IP address in a
separate HTTP header when making requests to the Drupal server.

This module then reads the value of this header and sets the `X-Forwarded-For`
header to the value of the header. This makes Drupal core aware of the
client's IP address.

### Functionality

The following functionality is provided:

- Enable client IP remapping on a per consumer basis.
- Configure the name of the HTTP header to retrieve the client IP from.

## Requirements

The core module requires at least Drupal 10.3 or 11 and the following
contrib modules:

- [Consumers](https://www.drupal.org/project/consumers)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configuration is done in the respective consumer settings form.

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
