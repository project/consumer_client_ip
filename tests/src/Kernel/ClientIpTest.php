<?php

declare(strict_types=1);

namespace Drupal\Tests\consumer_client_ip\Kernel;

use Drupal\consumers\Entity\Consumer;
use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Test the module functionality.
 */
class ClientIpTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'consumer_client_ip',
    'user',
    'image',
    'system',
    'file',
  ];

  /**
   * The client.
   */
  protected ConsumerInterface $client;

  /**
   * The kernel.
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * The url to the test controller.
   */
  protected Url $url;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('consumer');
    $this->installConfig(['user']);

    $this->client = Consumer::create([
      'client_id' => 'test_client',
      'label' => 'test',
      'client_ip_header_mapping_enabled' => TRUE,
      'client_ip_header_name' => 'X-Client-IP',
    ]);
    $this->client->save();

    $this->httpKernel = $this->container->get('http_kernel');

    $this->url = Url::fromRoute('<front>');
  }

  /**
   * Test enabled mapping.
   */
  public function testWithClientIpHeader() {
    $clientIp = '117.5.61.211';

    // Success.
    $request = $this->getRequest(function (Request $request) use ($clientIp) {
      $request->headers->set('X-Client-IP', $clientIp);
    });

    $this->assertEquals($clientIp, $request->getClientIp());

    // Invalid header.
    $request = $this->getRequest(function (Request $request) use ($clientIp) {
      $request->headers->set('X-Wrong-Header', $clientIp);
    });

    $this->assertNotEquals($clientIp, $request->getClientIp());
  }

  /**
   * Test with header mapping disabled.
   */
  public function testWithDisabledConsumerSetting() {
    $this->client->set('client_ip_header_mapping_enabled', FALSE)->save();

    $clientIp = '117.5.61.211';

    $request = $this->getRequest(function (Request $request) use ($clientIp) {
      $request->headers->set('X-Client-IP', $clientIp);
    });

    $this->assertNotEquals($clientIp, $request->getClientIp());
  }

  /**
   * Test with specific source hader.
   */
  public function testWithDifferentHeaderConsumerSetting() {
    $this->client->set('client_ip_header_name', 'X-My-Special-IP')->save();

    $clientIp = '117.5.61.211';

    $request = $this->getRequest(function (Request $request) use ($clientIp) {
      $request->headers->set('X-My-Special-IP', $clientIp);
    });

    $this->assertEquals($clientIp, $request->getClientIp());
  }

  /**
   * Request client ip.
   *
   * Makes a request to the test controller which returns
   * the client ip of the given request.
   */
  protected function getRequest(?\Closure $beforeRequest) {
    $request = Request::create(
      uri: $this->url->toString(),
      method: 'GET',
    );

    // Set 127.0.0.1 to a trusted proxy for symfony to read the
    // client ip from the x-forwarded-for header.
    $request->setTrustedProxies(['127.0.0.1'], Request::HEADER_X_FORWARDED_FOR);

    $request->headers->set('X-Consumer-ID', 'test_client');

    if ($beforeRequest) {
      $beforeRequest($request);
    }

    // Handling the request here runs the event subscriber.
    // The subscriber modifies this $request's header.
    $this->httpKernel->handle($request);

    return $request;
  }

}
