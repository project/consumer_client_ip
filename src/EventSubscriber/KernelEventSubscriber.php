<?php

declare(strict_types=1);

namespace Drupal\consumer_client_ip\EventSubscriber;

use Drupal\consumers\Negotiator;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber for kernel requests.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * Construct new KernelEventSubscriber.
   */
  public function __construct(
    protected Negotiator $negotiator,
    protected LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onRequest', 999999],
    ];
  }

  /**
   * Handle incoming kernel requests.
   */
  public function onRequest(RequestEvent $event): void {
    $request = $event->getRequest();

    $consumer = $this->negotiator->negotiateFromRequest($request);
    if (!$consumer) {
      return;
    }

    $enabled = (boolean) $consumer->get('client_ip_header_mapping_enabled')->getString();
    if (!$enabled) {
      return;
    }

    $headerName = strtolower($consumer->get('client_ip_header_name')->getString());

    // Get client ip from header.
    $clientIp = $request->headers->get($headerName);
    if (!$clientIp) {
      return;
    }

    // If ip is 0.0.0.0 means, that the client ip could not be retrieved.
    if ($clientIp === '0.0.0.0') {
      $this->logger->critical('No valid client ip set on request: 0.0.0.0');
      return;
    }

    // Set client ip to x-forwarded-for header.
    $event->getRequest()->headers->set('X-Forwarded-For', $clientIp);
  }

}
